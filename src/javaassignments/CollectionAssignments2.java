package javaassignments;

import java.util.ArrayList;
import java.util.List;

public class CollectionAssignments2 {

	public static void main(String[] args) {
		
		List<Integer> ex = new ArrayList<Integer>();

		List<Integer> ex1 = new ArrayList<Integer>();

		ex.add(10);

		ex.add(20);

		ex.add(30);

		ex.add(40);

		ex.add(40);

		ex.add(60);

		ex.add(70);

		ex.add(80);

		ex.add(90);

		int v = ex.size();

		System.out.println(v);

		int x = ex.get(3);

		System.out.println(x);

		for (int i = 0; i < ex.size(); i++)
		{
			System.out.println(ex.get(i));
		}

		ex.remove(3);

		System.out.println(ex);

		ex.add(2, 100);

		System.out.println(ex);

		ex.set(2, 100);

		System.out.println(ex);

		boolean w = ex.contains(30);

		System.out.println(w);

		boolean y = ex.contains(100);

		System.out.println(y);

		int y1 = ex.indexOf(30);

		System.out.println(y1);

		int z = ex.lastIndexOf(40);

		System.out.println(z);

		ex1.addAll(ex);

		System.out.println(ex);

		System.out.println(ex1);

		ex1.removeAll(ex);

		System.out.println(ex1);

		ex1.retainAll(ex);

		System.out.println(ex1);

		ex1.addAll(ex);

		System.out.println(ex);

	}

	}


